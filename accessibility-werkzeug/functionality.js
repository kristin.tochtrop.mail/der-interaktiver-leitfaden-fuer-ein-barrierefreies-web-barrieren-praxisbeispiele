    function toggleClassArray() {
        // array mit Klassennamen, was durchlaufen wird
        var classArray = this.state.accWebToolClassArray;
        var i;
        for (i = 0; i < classArray.length; i++) {
            let className = classArray[i];
            if (document.body.classList.contains(className)) {
                document.body.classList.remove(className);
                if (i == classArray.length - 1) {
                    //document.body.classList.add(classArray[0]);
                    return false;
                }
                else {
                    document.body.classList.add(classArray[i + 1]);
                    return classArray[i + 1];
                }
            }
        }
        document.body.classList.add(classArray[0]);
        return classArray[0];
    }

    function saveSettings(newValue) {
        let settings = document.body.classList.value;
        localStorage.setItem('name-var-local-storage', settings);
    }

   function  getStoredLevel() {
        const allSettings = localStorage.getItem('acc_settings');
        const setString = this.props.itemName + '-set-';
        try {
            return allSettings.charAt(allSettings.indexOf(this.props.itemName) + setString.length)
        } catch (e) {
            return 0;
        }
    }